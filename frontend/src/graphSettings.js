const graphSettings = {
  width: 900,
  height: 900,
  modes: {
    default: ["drag-canvas", "zoom-canvas", "drag-node"],
  },
  layout: {
    type: "dagre",
    direction: "LR",
  },
  defaultNode: {
    type: "node",
    labelCfg: {
      style: {
        fill: "#000000A6",
        fontSize: 15,
      },
    },
    style: {
      stroke: "#72CC4A",
      width: 150,
    },
  },
  defaultEdge: {
    type: "polyline",
  },
};
export default graphSettings;
