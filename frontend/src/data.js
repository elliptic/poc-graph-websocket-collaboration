export const data = {
  nodes: [
    {
      id: "1",
      label: "test1",
    },
    {
      id: "2",
      label: "test2",
    },
    {
      id: "3",
      label: "test3",
    },
    {
      id: "4",
      label: "test4",
    },
    {
      id: "5",
      label: "test5",
    },
    {
      id: "6",
      label: "test6",
    },
    {
      id: "7",
      label: "test7",
    },
    {
      id: "8",
      label: "test8",
    },
    {
      id: "9",
      label: "test9",
    },
  ],
  edges: [
    {
      source: "1",
      target: "2",
      data: {
        type: "name1",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "1",
      target: "3",
      data: {
        type: "name2",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "2",
      target: "5",
      data: {
        type: "name1",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "5",
      target: "6",
      data: {
        type: "name2",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "3",
      target: "4",
      data: {
        type: "name3",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "4",
      target: "7",
      data: {
        type: "name2",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "1",
      target: "8",
      data: {
        type: "name2",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
    {
      source: "1",
      target: "9",
      data: {
        type: "name3",
        amount: "100,000,000,00 元",
        date: "2019-08-03",
      },
    },
  ],
};

export default data;