import "./App.css";
import React, { useEffect, useState } from "react";
import data from "./data";
import ReactDOM from "react-dom";
import G6 from "@antv/g6";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { v4 as uuidv } from "uuid";
import graphSettings from "./graphSettings";
import debounce from "./debounce";
import SimplePeer from "simple-peer";

const App = () => {
  const ref = React.useRef(null);
  const [graph, setGraph] = useState(null);
  const [ws, setWs] = useState(null);

  // simple-peer data
  const [myData, setmyData] = useState(null);
  const [connectionsId, setConnectionsId] = useState([]);
  const [peers, setPeers] = useState([]);

  const setAddHandler = () => {
    if (graph) {
      graph.on("afteradditem", (evt) => {
        ws.send(JSON.stringify({ type: "add", model: evt.item.getModel() }));
      });
    }
  };

  const handleDrag = debounce((evt) => {
    if (evt.item) {
      ws.send(JSON.stringify({ type: "drag", model: evt.item.getModel() }));
    }
  }, 10);

  const setDragHandler = () => {
    graph.on("drag", handleDrag);
  };

  const setEndDragHandler = () => {
    graph.on("dragnodeend", (evt) => {
      if (evt.items.length > 0) {
        ws.send(
          JSON.stringify({ type: "drag", model: evt.items[0].getModel() })
        );
      }
    });
  };

  useEffect(() => {
    if (ws) {
      ws.onmessage = (message) => {
        try {
          const parsedMessage = JSON.parse(message.data);
          console.log("dostaje wiadomosc", parsedMessage);
          switch (parsedMessage?.type) {
            case "drag": {
              graph.off("drag");
              graph.updateItem(parsedMessage.model.id, parsedMessage.model);
              setDragHandler();
              break;
            }
            case "add": {
              graph.off("afteradditem");
              graph.addItem("node", parsedMessage.model);
              setAddHandler();
              break;
            }

            case "graphUpdate": {
              const graphData = graph.save();
              if (graphData.nodes.length === 0) {
                graph.off("afteradditem");
                graph.read(data);
                setAddHandler();
              }
              console.log("graphData", graphData);
              ws.send(
                JSON.stringify({ type: "currentGraph", graph: graph.save() })
              );
              break;
            }

            case "currentGraph": {
              graph.off("afteradditem");
              graph.destroyLayout();
              graph.read(parsedMessage.graph);
              setAddHandler();
              break;
            }

            // simple peer setup
            case "myId": {
              console.log("myId", parsedMessage);
              setmyData({
                id: parsedMessage.id,
                initiator: parsedMessage.initiator,
              });
              setConnectionsId(parsedMessage.ids);
              break;
            }

            case "newConnection": {
              setConnectionsId((origin) => [...origin, parsedMessage.id]);
              const newPeer = new SimplePeer({
                initiator: myData.initiator,
              });
              newPeer.on("error", console.error);
              newPeer.on("signal", (data) => {
                ws.send(
                  JSON.stringify({
                    type: "signal",
                    id: myData.id,
                    data,
                  })
                );
              });
              newPeer.on("data", (data) => console.log(data));
              setPeers((origin) => [...origin, newPeer]);
              break;
            }
            case "signal": {
              peers?.forEach?.((peer) => {
                peer.signal("test");
              });
              break;
            }
            default: {
            }
          }
        } catch (e) {
          console.error(e);
        }
      };
    }
  }, [ws, graph, myData]);

  useEffect(() => {
    try {
      let tempWS = new W3CWebSocket("ws://127.0.0.1:5000");
      tempWS.onopen = () => {
        setWs(tempWS);
      };
    } catch {
      window.alert("Unable to establish connection");
    }
  }, []);

  useEffect(() => {
    if (ws && !graph) {
      setGraph(
        new G6.Graph({
          ...graphSettings,
          container: ReactDOM.findDOMNode(ref.current),
        })
      );
    }
  }, [ws, graph]);

  useEffect(() => {
    if (ws && graph) {
      ws.send(JSON.stringify({ type: "graphUpdate" }));
    }
  }, [graph, ws]);

  useEffect(() => {
    if (graph && ws) {
      setAddHandler();
      setDragHandler();
      setEndDragHandler();
    }
  }, [graph, ws]);

  const handleClick = (e) => {
    const model = {
      id: uuidv(),
      label: "node",
      address: "cq",
      x: 200,
      y: 150,
      style: {
        fill: "blue",
      },
    };
    graph.addItem("node", model);
  };
  return (
    <div className="container">
      <button onClick={handleClick}>ADD NODE</button>
      <div ref={ref}></div>
    </div>
  );
};

export default App;
