const express = require("express");
const ws = require("ws");
const app = express();
const uuid = require("uuid");

const parseResponse = (res) => {
  try {
    return res.toString("utf-8");
  } catch (e) {
    return null;
  }
};

const liveConnections = [];

const wsServer = new ws.Server({ noServer: true });

wsServer.on("connection", (socket) => {
  const id = uuid.v4();

  socket.send(
    JSON.stringify({
      type: "myId",
      id,
      initiator: liveConnections.length === 0,
      ids: liveConnections.map((conn) => conn.id),
    })
  );

  socket.on("message", (message) => {
    liveConnections.forEach((connection) => {
      if (socket !== connection.socket) {
        connection.socket.send(parseResponse(message));
      }
    });
  });

  liveConnections.forEach((conn) =>
    conn.socket.send(
      JSON.stringify({
        type: "newConnection",
        id,
      })
    )
  );
  liveConnections.push({ socket, id });
});

const server = app.listen(5000);
server.on("upgrade", (request, socket, head) => {
  wsServer.handleUpgrade(request, socket, head, (socket) => {
    wsServer.emit("connection", socket, request);
  });
});
